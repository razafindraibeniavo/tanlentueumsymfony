## Test talentueum


## Installation des packages
`composer install`

## Servir l'applicaion
`symfony server:start --port 9070`

## Service graphQl disponible sur
`https://localhost:9070/graphiql`

## Fixtures à exécuter
`symfony console doctrine:fixtures:load`

## query to execute in graphql
```json
//Récupérer les informations d’un utilisateur
query{
  participant(id:344){
      id,
      firstname,
      lastname,
  }
}

//Récupérer les informations d’une discussion
//Récupérer les messages d’une discussion
query{
    thread(id:330){
        subject,
        messages{
          id
        }
    }
}

//Publier un nouveau message dans une discussion
mutation{
    publierMessage(content: {content: "Bonjour" , thread: 331,user: 344}){
      content
    }
}

//Créer une nouvelle discussion
mutation{
    createDiscussion(content:{subject: "Développement informatique"}){
        id,
        subject
    }
}

//Connaître le nombre de messages non lus.
query{
  getNotReadMessage
}
```
### Thank you !
