<?php

namespace App\DataFixtures;

use App\Entity\Message;
use App\Entity\Metadata;
use App\Entity\Thread;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Generator;
use Faker;
use Marvin255\RandomStringGenerator\Generator\RandomStringGenerator;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');
        $threads = [];
        //Créer des threads d'abord
        for($t= 1 ; $t<=2; $t++) {
            $thread = new Thread();
            $thread->setSubject($faker->sentence(10));
            $manager->persist($thread);
            $manager->flush();
            $threads [] = $thread;
        }

        //Attachée à un utilisateur nouvellement crée les précédentes threads
        for ($u = 1; $u <= 10; $u++) {
            $newParticipant = new User();
            $newParticipant->setEmail($faker->email);
            $newParticipant->setPassword($faker->password(10));
            $newParticipant->setRoles(["ROLE_PARTICIPANT"]);
            $newParticipant->setLastname($faker->lastName());
            $newParticipant->setFirstname($faker->firstName);
            //Créez une métadata pour une discussion crée par un utilisateur .
            foreach($threads as $thread){
                $newParticipant->addThread($thread);
                //Créé un message sur le thread que l'utilisateur a posté
                for ($m = 1; $m <= 1; $m++) {
                    $message = new Message();
                    $message->setContent($faker->paragraph(1));
                    $message->setDate(new \DateTime(date("Y-m-d")));
                    $message->setThread($thread);
                    $newParticipant->addMessage($message);
                }
            }
            foreach($newParticipant->getMessages() as $message){
                $metadata = new Metadata();
//                $metadata->setReaddate(new \DateTime(date("Y-m-d")));
                $message->addMetadata($metadata);
            }

            $manager->persist($newParticipant);
            $manager->flush();
        }
    }
}
