<?php

namespace App\Service;

use App\Entity\Category;
use App\Entity\Message;
use App\Entity\Metadata;
use App\Entity\Produit;
use App\Entity\Thread;
use App\Entity\User;
use App\Repository\CategoryRepository;
use App\Repository\MetadataRepository;
use App\Repository\ThreadRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Marvin255\RandomStringGenerator\Generator\RandomStringGenerator;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class MutationService
{
    public function __construct(
        private EntityManagerInterface $em,
        private RandomStringGenerator $randomStringGenerator,
        private ThreadRepository $threadRepository,
        private UserRepository $userRepository,
        private MetadataRepository $metadataRepository,
        private CategoryRepository $categoryRepository
    )
    {
    }

    public function createParticipant(array $participantDetails) : User{
        $newParticipant = new User();
        $newParticipant->setEmail($participantDetails["email"]);
        $newParticipant->setPassword($this->randomStringGenerator->password(10));
        $newParticipant->setRoles(["ROLE_PARTICIPANT"]);
        $newParticipant->setFirstname($participantDetails["firstname"]);

        $this->em->persist($newParticipant);
        $this->em->flush();
        return $newParticipant;
    }

    public function publierMessageThread(array $content){
        $thread = $this->threadRepository->find($content["thread"]);
        $participant = $this->userRepository->find($content["user"]);
        $message = new Message();
        $message->setContent($content["content"]);
        $message->setDate(new \DateTime("now"));
        $message->setThread($thread);
        $participant->addMessage($message);

        $metadata = new Metadata();
//        $metadata->setReaddate(new \DateTime(date("Y-m-d")));
        $message->addMetadata($metadata);

        $this->em->persist($participant);
        $this->em->flush();
        return $message;
    }

    public function createDiscussion($content){
        $thread = new Thread();
        $thread->setSubject($content["subject"]);
        $this->em->persist($thread);
        $this->em->flush();
        return $thread;
    }

    public function createCategory($content){
        $category = new Category();
        $category->setName($content["name"]);
        $this->em->persist($category);
        $this->em->flush();
        return $category;
    }

    public function createProduit($content){
        $produit = new Produit();
        $produit->setName($content["name"]);
        $produit->setDesignation($content["designation"]);
        $produit->setDesignation($content["designation"]);
        $category = $this->categoryRepository->find($content["category"]) ?? null;
        $produit->setCategory($category);
        $this->em->persist($produit);
        $this->em->flush();
        return $produit;
    }

}