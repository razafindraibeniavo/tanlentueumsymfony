<?php

namespace App\Service;

use App\Entity\Thread;
use App\Entity\User;
use App\Repository\MetadataRepository;
use App\Repository\ProduitRepository;
use App\Repository\ThreadRepository;
use App\Repository\UserRepository;

class QueryService
{
    public function __construct(
        private UserRepository $userRepository,
        private ThreadRepository $threadRepository,
        private MetadataRepository $metadataRepository,
        private ProduitRepository $produitRepository

    ){

    }

    public function findParticipant(int $userId) : User{
        return $this->userRepository->find($userId);
    }

    public function findThread(int $threadId): Thread{
        return $this->threadRepository->find($threadId);
    }

    public function getAllParticipants(){
        return $this->userRepository->findAll();
    }

    public function getNotReadMessage(){
        $allMetadata = $this->metadataRepository->findBy(["readdate" => null]);
        return count($allMetadata);
    }

    public function getProduits(int $produitId){
        return $this->produitRepository->findBy(["category"=> $produitId]);
    }
}