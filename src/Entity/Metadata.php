<?php

namespace App\Entity;

use App\Repository\MetadataRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MetadataRepository::class)]
class Metadata
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $readdate = null;

    #[ORM\ManyToOne(inversedBy: 'metadatas')]
    private ?Message $message = null;

    #[ORM\OneToOne(inversedBy: 'metadata', cascade: ['persist', 'remove'])]
    private ?User $user = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReaddate(): ?\DateTimeInterface
    {
        return $this->readdate;
    }

    public function setReaddate(?\DateTimeInterface $readdate): self
    {
        $this->readdate = $readdate;

        return $this;
    }

    public function getMessage(): ?Message
    {
        return $this->message;
    }

    public function setMessage(?Message $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
