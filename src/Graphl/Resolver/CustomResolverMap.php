<?php

namespace App\Graphl\Resolver;

use App\Service\QueryService;
use Overblog\GraphQLBundle\Resolver\ResolverMap;
use App\Service\MutationService;
use ArrayObject;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\ArgumentInterface;

class CustomResolverMap extends ResolverMap
{
    public function __construct(
        private QueryService $queryService,
        private MutationService $mutationService
    )
    {
    }

    protected function map()
    {
        return [
            'RootQuery' => [
                self::RESOLVE_FIELD => function (
                    $value,
                    ArgumentInterface $args,
                    ArrayObject $context,
                    ResolveInfo $info
                ) {
                    return match ($info->fieldName) {
                        //Récupérer les informations d’un utilisateur
                        'participant' => $this->queryService->findParticipant((int)$args['id']),
                        'participants' => $this->queryService->getAllParticipants(),
                        //Récupérer les informations d’une discussion
                        //Récupérer les messages d’une discussion
                        'thread' => $this->queryService->findThread((int)$args['id']),
                        // Connaître le nombre de messages non lus.
                        'getNotReadMessage' => $this->queryService->getNotReadMessage($args),
                        'produits' => $this->queryService->getProduits((int)$args['id']),
                        default => null
                    };
                },
            ],
            'RootMutation' => [
                self::RESOLVE_FIELD => function (
                    $value,
                    ArgumentInterface $args,
                    ArrayObject $context,
                    ResolveInfo $info
                ) {
                    return match ($info->fieldName) {
                        'createParticipant' => $this->mutationService->createParticipant($args['participant']),
                        //Publier un nouveau message dans une discussion
                        'publierMessage' => $this->mutationService->publierMessageThread($args['content']),
                        // Créer une nouvelle discussion
                        'createDiscussion' => $this->mutationService->createDiscussion($args['content']),
                        'createCategory' => $this->mutationService->createCategory($args['content']),
                        'createProduit' => $this->mutationService->createProduit($args['content']),
                        default => null
                    };
                },
            ],
        ];
    }
}